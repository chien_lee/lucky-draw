@extends('layouts.master')

@section('title', 'User')

@section('css')
<style type="text/css">
</style>
@stop

@section('content')

<div class="container">
	<div class="row cls-event-poll">
	    <div class="col-sm-8">
	    	<h2>{{ $polling_data['title'] }}</h2>
	        <div class="panel panel-default">
	            <div class="panel-heading">
	            	<h3 class="panel-title">{{ $polling_data['question'] }}</h3>
	            </div>
	            <div class="panel-body"> 
	            	@php
	            		$element_type = $polling_data['type'];
	            	@endphp           	
		
	                <ul class="list-unstyled">
	                	@foreach($polling_data['options'] as $key => $value)
	                		<li>
			                    <div class="{{ $element_type }}">
			                        <label for="poll-opt-id-{{ $value['id'] }}" class="control-label">
			                            <input type="{{ $element_type }}" id="poll-opt-id-{{ $value['id'] }}" value="{{ $value['value'] }}" name="option_id[]" class="poll-opt" /> 
			                            {{ $value['value'] }}
			                        </label>
			                    </div>
			                </li>
	                	@endforeach
	                </ul>
	            <br />
					<button id="vote" class="btn btn-success">Vote</button> 
	            </div>
	        </div>
	    </div>
	</div>
</div>

@stop

@section('javascript')
<script type="text/javascript">
	$("#vote").click(function(){
		var UserPollingOptions = {};		
		var jsonObject 		   = [];
		var poll_id  		   = "{{ $polling_data['id'] }}";
		var	user_id 		   = "{{ Request::session()->get('user_id') == '' ? Request::session()->get('user_id') : '' }}";

		// multiple selection checking
		if($("#poll-opt").length == 1)
		{
			// radio method
			var poll_opt_id 	  = $("#poll-opt option:selected").val();
			var option_value_text = $("#poll-opt option:selected").text();

			// selection checking
			if(poll_opt_id == '')
			{
				alert("Please make a selection!");
				return false;
			}

			// collect data
			UserPollingOptions = {
				user_id				: user_id,
				polling_option_id   : event_poll_opt_id,
				option_value 		: option_value_text
			};	

			jsonObject.push(UserPollingOptions);	
		}
		else
		{
			// checkbox method
			var poll_opt_ids = $(".poll-opt:checked").map(function(){return $(this).attr('id').substr(12);}).get();	

			// selection checking
			if(poll_opt_ids == '')
			{
				alert("Please make a selection!");
				return false;
			}

			// loop selection data
			for(var i = 0; i < poll_opt_ids.length; i++)
			{
				UserPollingOptions = {
					user_id      		: user_id,
					polling_option_id   : poll_opt_ids[i],
					option_value        : $("#poll-opt-id-" + poll_opt_ids[i]).val()
				};	

				jsonObject.push(UserPollingOptions);	
			}	
		}	

		console.log(jsonObject);
		return false;

		// insert data
		$.ajax({
			type: "POST",				
			dataType: 'json',
			data: {'UserPollingOption': jsonObject},			
			complete: function (data) {		
				notification("Successfully vote.", "Success");
				// return poll result page        				
				window.location = "user/poll_results/" + poll_id;			
			}		
		});	

	});
</script>
@stop