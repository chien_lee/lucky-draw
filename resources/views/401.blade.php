@extends('layouts.master')

@section('title', 'Ops, Access Denied')
@section('css')
<style type="text/css">

</style>
@stop

@section('content')
<div class="empty-page-parent text-center">
	<div class="empty-page-child">
		<div><h1>Oops!</h1></div>
		<div>401 Access Denied</div>
		<div>Stop! You are trying to access restricted page.</div>
		<a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Take Me Home </a>
	</div>
</div>
@stop