<link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.min.css') }}">
<script src="{{ URL::asset('public/js/FontAwesome/svg-with-js/js/fontawesome-all.min.js') }}"></script>  

<style type="text/css">
.error-page {
    text-align: center;
    padding-top: 1em;
    padding-bottom: 1em;
    height: 100%
}
@media screen and (max-width:767px) {
    .error-page {
        height: auto
    }
}
.error-page.update-browser-page .text-block {
    max-width: 45em;
    margin: 0 auto
}
.error-page.update-browser-page .browsers a {
    display: block
}
.error-page.update-browser-page .browsers img {
    max-width: 100%
}
.error-page .table-block {
    display: table;
    height: 100%;
    width: 100%
}
.error-page .table-block .cell-block {
    display: table-cell;
    text-align: center;
    vertical-align: middle
}
.error-page .error-subtitle {
    color: #a6a6a6
}

body {
    font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
    line-height: 1.428571429;
    color: #666;
    background-color: #f2f2f2
}

.fa-10x {
    font-size: 15em !important;
}
</style>

<div class="container error-page update-browser-page">
    <div class="table-block">
        <div class="cell-block">
            <i class="far fa-frown fa-10x"></i>
            <h2>Outdated Browser Detected</h2>
            <div class="text-block">
                <p>Our website has detected that you are using an outdated browser. Using your current browser will prevent you from accessing features on our website. An update is required.</p>
                <br />
                <p><strong>Use the links below to download a new browser or upgrade your existing one:</strong></p>
                <br />
            </div>
            <div class="row browsers">
                <div class="col-xs-4">
                    <a href="http://www.google.com/chrome" target="_blank">
                        <img src="/public/images/chrome_icon.png" alt="Chrome" />
                        <br />
                        <span class="sm">Chrome</span>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="http://www.mozilla.com/en-US/firefox/all.html" target="_blank">
                        <img src="/public/images/firefox_icon.png" alt="Firefox" />
                        <br />
                        <span class="sm">Firefox</span>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="http://www.apple.com/safari/download/" target="_blank">
                        <img src="/public/images/safari_icon.png" alt="Safari" />
                        <br />
                        <span class="sm">Safari</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


