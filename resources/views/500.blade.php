@extends('layouts.master')

@section('title', 'Ops, Requested page not found!')
@section('css')
<style type="text/css">
</style>
@stop

@section('content')
<div class="empty-page-parent text-center">
	<div class="empty-page-child">
		<div><h1>Oops!</h1></div>
		<div>500 Internal Error</div>
		<div>Ops! Looks like there's something happened. If you see this page please contact administrator.</div>
		<a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Take Me Home </a>
	</div>
</div>
@stop