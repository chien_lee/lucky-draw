@extends('layouts.master')

@section('css')
<style type="text/css">
	.container{
		margin-top: 1%;
	}

	/* red star on required fileds */
	.form-group.required .control-label:after {
		content:"*";
		color:red;
	}
</style>
@stop

@section('content')
<div class="center-block container">
	<div class="col-md-6">
		<h2>Lucky Draw</h2>
	</div>

	<div class="col-md-6 text-right">
		<div class="custom-action-class" style="margin-top:20px;">
			 <a href="/logout" class="btn btn-sm btn-warning"><i class="fa fa-sign-out-alt"></i> Logout</a>
		</div>
	</div>

	<hr>
	<div class="row form-horizontal col-md-12" style="margin-bottom: 2%;">
		<div class="form-group required">
			<label for="PrizeType" class="col-md-2 col-sm-6 col-xs-3 control-label">Prize Type</label>
			<div class="col-md-6 col-sm-6 col-xs-9">
				<select name="prize" id="prize" style="width: 45%;">
					<option value="" selected="selected">Please select</option>
					@foreach($prize_data as $key => $value)
						<option  data-prize-id="{{ $value['id'] }}" value="{{ $value['prize_type'] }}">{{ $value['name'] }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group required">
			<label for="GenerateRandomly" class="col-md-2 col-sm-6 col-xs-3 control-label">Generate Randomly</label>
			<div class="col-md-6 col-sm-6 col-xs-9">
				<select name="generate_randomly" id="generate_randomly" style="width: 45%;">
					<option value="" selected="selected">Please select</option>
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="WinningNumber" class="col-md-2 col-sm-6 col-xs-3 control-label">Winning Number</label>
			<div class="col-md-3 col-sm-6 col-xs-9">
				<input name="winning_number" id="winning_number" class="form-control" type="text">
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-2 col-sm-6 col-xs-3"></div>
			<div class="col-md-6 col-sm-6 col-xs-9">
				<button id="draw" class="btn btn-primary"><i class="fa fa-sync"></i>  Draw</button> 
			</div>
		</div>
	</div>

	<h2>Report</h2>
	<hr>
	<table id="tbl_report" class="table table-bordered" width="100%">
		<thead>
			<th align="center">#</th>
			<th align="center">Name</th>
			<th align="center">Prize</th>
			<th align="center">Drawn By</th>
			<th align="center">Date Time</th>
		</thead>
	</table>
</div>
@stop

@section('javascript')
<script type="text/javascript">
	$("#draw").on("click", function(){
		var button 		  =  $(this);
		var button_text   =  button.text();
		var prize_type    =  $("#prize").val();
		var prize_id      =  $("#prize option:selected").data('prize-id');
		var is_random     =  $("#generate_randomly").val();
		var win_no	      =  $("#winning_number").val();
		var admin_id      =  "{{ Request::session()->get('authentication') != '' ? Request::session()->get('authentication') : '' }}"
		var data 	      =  [];

		// prize validation
		if(!prize_type){
			notification("Please selecet a prize", "Danger");
			return false;
		}

		// generate randomly validation
		if(!is_random){
			notification("Please select a generate randomly", "Danger");
			return false;
		}

		// grand prize validation
		if(prize_type == 'grand'){
			if(is_random == 'no'){
				notification("Grand Prize Only For Randomly Picked", "Danger");
				return false;
			}
		}
		else{
			// check winning no
			if(is_random == 'no'){
				if(!win_no){
					notification("Please enter winning number", "Danger");
					return false;
				}
			}
		}

		// process
		$.ajax({
			type: "POST",               
			url: "draw_process",
			data: {
				prize_type  : prize_type, 
				is_random   : is_random, 
				prize_id    : prize_id, 
				admin_id    : admin_id, 
				win_no      : win_no,
				_token      : '{{ csrf_token() }}'
			},
			beforeSend: function(){
				button.html(loading_src);
				button.attr('disabled', true);
			},
			success: function(data){
				button.html(button_text);
				button.attr('disabled', false);

				if(data.status == 200){
					if ( $.fn.DataTable.isDataTable( '#tbl_report' ) ) {  
						$('#tbl_report').DataTable().settings()[0].jqXHR.abort();
						$('#tbl_report').DataTable().destroy();
					}
					table_init();
					alert("Name of Winner: " + data.result['name']); 
				}
				else{
					notification(data.message, "Danger");
				}
			},
			error: function(xhr, exception){
				button.html(button_text);
				button.attr('disabled', false);

				if(xhr.status == 401){
					notification("401 Unathorized Action", "Danger");
				}
			}
		});     
	});

	table_init();

	function table_init(){
		$('#tbl_report').dataTable({
            dom: 'lrtp',
            "pageLength": 10,
            "lengthMenu": [
                [ 10, 20, 30 ],
                [ '10', '20', '30' ]
            ],
            "processing": true,
            "bStateSave": true,
            "serverSide": true,
            "language": {
                  "emptyTable": "There are no search results"
            },
            "ajax": {
                url: "/draw_result",
                type: "GET",
                beforeSend:function(){
                },
                dataFilter: function(reps) {
					return reps;
				}
            },
        });
	}
</script>
@stop