@extends("layouts.master")

@section("css")
<style type="text/css">
	body{
		background:#f4f4f4 url({{ URL::asset('public/images/bg.gif') }}) repeat top left;
	} 
	.login-container{
		margin-top: 5%;
		margin-bottom: 5%;
	}
	.login-form-1{
		padding: 5%;
		background-color: #f6f6f6;
		box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
	}
	.login-form-1 h3{
		text-align: center;
		color: #333;
	}
	.login-container form{
		padding: 10%;
	}
	.btnSubmit
	{
		width: 50%;
		border-radius: 1rem;
		padding: 1.5%;
		border: none;
		cursor: pointer;
	}
	.login-form-1 .btnSubmit{
		font-weight: 600;
		color: #fff;
		background-color: #0062cc;
	}
	.login-form-1 .ForgetPwd{
		color: #0062cc;
		font-weight: 600;
		text-decoration: none;
	}
</style>
@stop

@section("content")
<div class="container login-container" align="center">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 login-form-1">
			<h3>Admin Login</h3>
			<form  method="post">
				<div class="form-group">
					<input type="text" class="form-control" required placeholder="Email *" name="email" value="" />
				</div>
				<div class="form-group">
					<input type="password" class="form-control" required placeholder="Password *" name="password" value="" />
				</div>
				
				<div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				</div>
				<br>

				<div class="form-group">
					<input type="submit" class="btnSubmit" value="Login" />
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
