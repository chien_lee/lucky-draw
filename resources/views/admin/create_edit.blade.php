@extends('layouts.master')

@section('title', 'Admin')

@section('css')
<style type="text/css">
	/* red star on required fileds */
	.form-group.required .control-label:after {
		content:"*";
		color:red;
	}
</style>
@stop

@section('content')
	<div class="center-block container">
	<h2>Create Question</h2>
	<hr>
	<div class="row">
		<form action="" class="form-horizontal" role="form" id="EventSurveyAddForm">
			<div class="form-group required">
				<label for="PollingTitle"  class="col-md-2 col-xs-8 control-label">Title</label>
				<div class="col-md-6 col-sm-4">
					<input name="title" class="form-control" required="required" type="text">
				</div>
			</div>

			<div class="form-group required">
				<label for="PollingQuestion" class="col-md-2 col-xs-8 control-label">Question</label>
				<div class="col-md-6 col-sm-4">
					<input name="question" class="form-control" required="required" type="text">
				</div>
			</div>

			<div class="form-group required">
			   <label for="PollingStatus" class="col-md-2 col-xs-8 control-label">Status</label>
			   <div class="col-md-6 col-sm-4">
			   	<select name="status" id="status" style="width: 30%;">
			   		<option value="open" selected="selected">Open</option>
			   		<option value="close">Close</option>
			   	</select>
			   </div>
			</div>

			<div class="form-group required">
			   <label for="PollingStatus" class="col-md-2 col-xs-8 control-label">Show Statistic</label>
			   <div class="col-md-6 col-sm-4">
			   	<select name="show_statistic" id="show_statistic" style="width: 30%;">
			   		<option value="1" selected="selected">Yes</option>
			   		<option value="0">No</option>
			   	</select>
			   </div>
			</div>

			<div class="form-group required">
			   <label for="PollingStatus" class="col-md-2 col-xs-8 control-label">Statistic Type</label>
			   <div class="col-md-6 col-sm-4">
			   	<select name="statistic_type" id="statistic_type" style="width: 30%;">
			   		<option value="bar" selected="selected">Bar Chart</option>
			   		<option value="pie">Pie Chart</option>
			   	</select>
			   </div>
			</div>

			<div class="row form-group required">
			   <label class="col-xs-12 col-md-2 control-label">Field Type</label>
			   <div class="col-md-8 col-sm-8">
			      <select name="field_type" id="field_type" style="width: 30%;">
			         <option value="checkbox" selected="selected">Checkbox</option>
			         <option value="radio">Radio</option>
			      </select>
			   </div>
			</div>

			<div class="row form-group required">
			   <label class="col-xs-12 col-md-2 control-label">Show Public</label>
			   <div class="col-md-8 col-sm-8">
			      <select name="show_public" id="show_public" style="width: 30%;">
			         <option value="1" selected="selected">Yes</option>
			         <option value="0">No</option>
			      </select>
			   </div>
			</div>

			<div class="row form-group required">
			   <label class="col-xs-12 col-md-2 control-label">Editable</label>
			   <div class="col-md-8 col-sm-8">
			      <select name="editable" id="editable" style="width: 30%;">
			         <option value="1" selected="selected">Yes</option>
			         <option value="0">No</option>
			      </select>
			   </div>
			</div>

			<div class="form-group">
				<label for="PollingAccessCode" class="col-md-2 col-xs-8 control-label">Access Code</label>
				<div class="col-md-6 col-sm-6">
					<input name="access_code" class="form-control" type="text">
				</div>
			</div>

			<div class="row form-group required">
			   <label class="col-xs-12 col-md-2 control-label">Field Type</label>
			   <div class="col-md-8 col-sm-8">
			      <select name="field_type" id="field_type" style="width: 30%;">
			         <option value="checkbox" selected="selected">Checkbox</option>
			         <option value="radio">Radio</option>
			      </select>
			   </div>
			</div>

			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Answer Options</label>
				<div class="col-sm-6">
					<ul class="draggableItem list-unstyled ui-sortable" id="optionitem_1">
						<li class="item" id="1001" alt="1">
							<div class="input-group" id="1_option_1"><span class="input-group-addon"><input type="checkbox" name="box_2" disabled></span>
								<input type="text" name="polling_options[]" class="form-control">
									<a class="input-group-addon" id="remove_1" onclick="removeOption(this, 2)">
										<span class="fa fa-minus-circle"></span>
									</a>
								<input type="hidden" name="polling_options[]" id="itemOrder_2_1001" value="1" style="width:40px;" class="form-control">
							</div>
						</li>
						<li class="item" id="1002" alt="2" style="">
							<div class="input-group" id="2_option_1"><span class="input-group-addon"><input type="checkbox" name="box_2" disabled></span>
								<input type="text" name="polling_options[]" class="form-control">
									<a class="input-group-addon" id="remove_1" onclick="removeOption(this, 2)">
										<span class="fa fa-minus-circle"></span>
									</a>
								<input type="hidden" name="polling_options[]" id="itemOrder_2_1002" value="1" style="width:40px;" class="form-control">
							</div>
						</li>
						<li class="item" id="1003" alt="2">
							<div class="input-group" id="3_option_1"><span class="input-group-addon"><input type="checkbox" name="box_2" disabled></span>
								<input type="text" name="polling_options[]" class="form-control">
									<a class="input-group-addon" id="remove_1" onclick="removeOption(this, 2)">
										<span class="fa fa-minus-circle"></span>
									</a>
								<input type="hidden" name="polling_options[]" id="itemOrder_2_1003" value="1" style="width:40px;" class="form-control">
							</div>
						</li>
					</ul>
					<a class="btn btn-default" onclick="addOption(this, 1)"><span class="fa fa-plus-circle"></span></a>
				</div>
			</div>
		</form>
	</div>
@stop

@section('javascript')
<script type="text/javascript">
	jQuery(function ($) {
		// Omit this to make the entire <li>...</li> draggable.
		var itemlist = $('.draggableItem');
		itemlist.sortable({
			update: function () {
				$('.item', itemlist).each(function (index, elem) {
					var itemID = $(this).attr('id');
					var counter = $(this).attr('alt');
					var $listItem = $(elem),
					newIndex = $listItem.index() + 1;

					// update order number
					$('#itemOrder_'+ counter + '_'+itemID).val(newIndex);
					$('#last_item_order_no_'+counter).val(newIndex);
				});
			}
		});
	});


	var addOne = 1;
	function addOption(elem, counter) {
		child 					= elem.parentNode.parentNode.parentNode.children;
		var number_of_nodes 	= child.length;
		var container_id 		= elem.parentNode.parentNode.id;
		var last_item_order_no  = $('#last_item_order_no_'+counter).val();
		last_item_order_no 		= parseFloat(last_item_order_no) + addOne;

		// for record option sequence
		$('#last_item_order_no_' + counter).val(last_item_order_no);

		// create new option
		var field_type  		= $('#field_type').val();
		var checkboxOption  	= $('li');
		checkboxOption.attr('class', 'item');
		checkboxOption.attr('id', counter+last_item_order_no);
		checkboxOption.attr('alt', counter);

		// checking option field type
		if (field_type === 'checkbox') {
        	checkboxOption.innerHTML = "<div class='input-group' id='option_"+ number_of_nodes +"'><span class='input-group-addon'><input type='checkbox' name='box_" + counter + "' disabled></span><input type='text' name='custom_field[" + counter + "][options][]' class='form-control' required='required'><a class='input-group-addon' id='remove_"+counter+"' onclick='removeOption(this, "+counter+", "+ counter +")'><span class='fa fa-minus-circle'></span></a><input type='hidden' name='polling_options[]' id='itemOrder_"+ counter +"_" + (counter + last_item_order_no) + "' value='" + last_item_order_no + "' style='width:40px;' class='form-control'></div>";
	    } 
	    else {
	        checkboxOption.innerHTML = "<div class='input-group' id='option_"+ number_of_nodes +"'><span class='input-group-addon'><input type='radio' name='box_" + counter + "' disabled></span><input type='text' name='custom_field[" + counter + "][options][]' class='form-control' required='required'><a class='input-group-addon' id='remove_"+counter+"' onclick='removeOption(this, "+counter+")'><span class='fa fa-minus-circle'></span></a><input type='hidden' name='polling_options[]' id='itemOrder_"+ counter +"_" + (counter + last_item_order_no) + "' value='" + last_item_order_no + "' style='width:40px;' class='form-control'></div>";
	    }

		var itemUldiv = $('#optionitem_'+counter);
		itemUldiv.append(checkboxOption.innerHTML);
	}

	function removeOption(elem, counter) {
		child 			 = elem.parentNode.parentNode.children;
		var container_id = elem.parentNode.parentNode.id;
		var totalOption  = $('body').find('.item').length;

		//validation for not allow remove all option
		if (totalOption == 1) {
			alert("Can't remove all option.");
			return false;
		}

		// remove selected element
		var child = elem.parentNode;
		elem.parentNode.parentNode.remove(child);

	}
</script>
@stop