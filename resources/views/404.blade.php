@extends('layouts.master')

@section('title', 'Ops, Requested page not found!')
@section('css')
<style type="text/css">
</style>
@stop

@section('content')
<div class="empty-page-parent text-center">
	<div class="empty-page-child">
		<div><h1>Oops!</h1></div>
		<div>404 Not Found</div>
		<div>Sorry, an error has occured, Requested page not found!</div>
		<a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Take Me Home </a>
	</div>
</div>
@stop