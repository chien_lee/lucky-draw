<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/css/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/css/datatables.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/css/select2-bootstrap.min.css') }}">

@section('css')
@show

<body>
    @section('body')
        @yield('content')
    @show
</body>

<script src="{{ URL::asset('public/js/jquery-3.1.1.min.js') }}"></script>  
<script src="{{ URL::asset('public/js/jquery-ui.min.js') }}"></script>  
<script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>  
<script src="{{ URL::asset('public/js/bootstrap-notify.js') }}"></script>  
<script src="{{ URL::asset('public/js/DataTables/datatables.min.js') }}"></script>  
<script src="{{ URL::asset('public/js/fontawesome/js/all.min.js') }}"></script>
<script src="{{ URL::asset('public/js/select2/select2.full.min.js') }}"></script>  


<script type="text/javascript">
    var loading_src = '<img height="20px" width="20px" src="{{ URL::asset('public/images/loading-spinner.gif') }}"/>';

    function notification(message, type){
		if(type.toLowerCase() == "success"){
			icon = 'fa fa-check';
		}else if(type.toLowerCase() == "danger"){
			icon = 'fa fa-times';
		}else if(type.toLowerCase() == "warning"){
			icon = 'fa fa-exclamation';
		}else{
			icon = 'fa fa-floppy-o';
		}
		if(message == "[error_perform]"){
			message = "Failed to perform action, if you see this message please contact administrator for assistance";
		}

		$.notify({
			icon: icon,
			message: message
		},{
			element: 'body',
			position: null,
			type: type,
			allow_dismiss: true,
			newest_on_top: false,
			showProgressbar: false,
			placement: {
				from: "top",
				align: "center"
			},
			offset: 20,
			spacing: 10,
			z_index: 10000,
			delay: 5000,
			timer: 1000,
			url_target: '_blank',
			mouse_over: null,
			animate: {
				enter: 'animated fadeInDown',
				exit: 'animated fadeOutUp'
			},
			onShow: null,
			onShown: null,
			onClose: null,
			onClosed: null,
			icon_type: 'class',
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
			'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
			'</div>'
		}); 
	}

    $("select").select2({
        theme: "bootstrap"
    });
</script>
@section('javascript')
@show
</html>