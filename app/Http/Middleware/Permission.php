<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use Closure;
use Input;
use DB;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        // Authentication
        $authentication_url = 'admin/login';

        if($request->session()->get('authentication') == null){
            return redirect($authentication_url);
        }

        return $next($request);
    }
}
