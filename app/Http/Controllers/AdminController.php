<?php
namespace App\Http\Controllers;
use App\Models\Admin;
use App\Models\PrizeType;
use App\Models\Member;
use App\Models\MemberNumber;
use App\Models\LuckyDrawResult;
use App\Http\Controllers\Controller;
use DB;
use Request;
use Hash;

class AdminController extends Controller
{
    public function lucky_draw(){
        DB::beginTransaction();
        try {
            // get prize data 
            $prize_data = PrizeType::get();

            DB::commit();
            return view('admin.lucky_draw', ['prize_data' => $prize_data]);
        } catch (\Exception $e) {
            DB::rollback();
        }
    }


    public function login(){
        if(Request::method() == 'POST'){
            DB::beginTransaction();
            
            try {
                $input = Request::input();
                
                // valify admin login
                if(isset($input['email'])){
                    $admin_acc = Admin::where('email', '=', $input['email'])
                    ->first();

                    if($admin_acc){
                        if($input['password'] != $admin_acc['password']){
                            DB::commit();
                            echo 'Invalid email and password.';
                            exit();
                        }
                        else{
                            Request::session()->put('authentication', $admin_acc['id']);                            
                        }
                    }
                }

                DB::commit();
                return redirect('/');

            } catch (\Exception $e) {
                DB::rollback();
            }
        }

        return view('admin.login');
    }
    
    public function draw_process(){
        if(Request::method() == 'POST'){
            DB::beginTransaction();
            
            try {
                $input            = Request::input();
                $prize_id         = $input['prize_id'];
                $prize_type       = $input['prize_type'];
                $is_random        = $input['is_random'];
                $admin_id         = $input['admin_id'];
                $win_no           = $input['win_no'];
                $conform_member   = [];

                // grand prize checking
                switch ($prize_type) {
                    case 'grand':
                        $conform_member = Member::rightJoin('member_numbers', 'member_numbers.member_id', '=', 'members.id')
                        ->where('members.is_drawn', '=', 0)
                        ->groupby('member_numbers.member_id')
                        ->having(DB::raw('count(member_numbers.member_id)'), '>=', DB::raw('(select max(count(member_numbers.member_id)) OVER () AS max_number FROM members
                            left join member_numbers on member_numbers.member_id = members.id
                            where members.is_drawn = 0
                            group by member_numbers.member_id
                            limit 1)'))
                        ->select('members.*', DB::raw('count(member_numbers.member_id) AS total_number'))
                        ->inRandomOrder()
                        ->limit(1)
                        ->get();

                        // conform member validation
                        if(count($conform_member) === 0){
                            DB::commit();
                            return $this->no_conform_member;
                        }

                        // insert result
                        $result       = new LuckyDrawResult();
                        $insert_data  = [
                            'prize_type_id'       => $prize_id,
                            'member_id'           => $conform_member[0]['id'],
                            'admin_id'            => $admin_id,
                            'winning_number'      => 'random',
                            'datetime'            => \Carbon\Carbon::now()
                        ];

                        $result_id = $result->insertGetId($insert_data);

                        // update member status
                        Member::where('id', '=', $conform_member[0]['id'])
                        ->update(['is_drawn' => 1]);

                        $data['result']  = $conform_member[0];

                    break;

                    case 'second':
                    case 'third' :
                        if($is_random == 'yes'){
                            $conform_member = Member::rightJoin('member_numbers', 'member_numbers.member_id', '=', 'members.id')
                            ->where('members.is_drawn', '=', 0)
                            ->groupby('member_numbers.member_id')
                            ->select('members.*')
                            ->inRandomOrder()
                            ->limit(1)
                            ->get();

                            // conform member validation
                            if(count($conform_member) === 0){
                                DB::commit();
                                return $this->no_conform_member;
                            }

                            // insert result
                            $result       = new LuckyDrawResult();
                            $insert_data  = [
                                'prize_type_id'       => $prize_id,
                                'member_id'           => $conform_member[0]['id'],
                                'admin_id'            => $admin_id,
                                'winning_number'      => 'random',
                                'datetime'            => \Carbon\Carbon::now()
                            ];

                            $result_id = $result->insertGetId($insert_data);

                            // update member status
                            Member::where('id', '=', $conform_member[0]['id'])
                            ->update(['is_drawn' => 1]);

                            $data['result']  = $conform_member[0];
                        }
                        else{
                            $conform_member = Member::rightJoin('member_numbers', 'member_numbers.member_id', '=', 'members.id')
                            ->where('members.is_drawn', '=', 0)
                            ->where('member_numbers.number', '=', $win_no)
                            ->groupby('member_numbers.member_id')
                            ->select('members.*')
                            ->first();

                            // conform member validation
                            if(!$conform_member){
                                DB::commit();
                                return $this->no_conform_member;
                            }

                            // insert result
                            $result       = new LuckyDrawResult();
                            $insert_data  = [
                                'prize_type_id'       => $prize_id,
                                'member_id'           => $conform_member['id'],
                                'admin_id'            => $admin_id,
                                'winning_number'      => $win_no,
                                'datetime'            => \Carbon\Carbon::now()
                            ];

                            $result_id = $result->insertGetId($insert_data);

                            // update member status
                            Member::where('id', '=', $conform_member['id'])
                            ->update(['is_drawn' => 1]);

                            $data['result']  = $conform_member;
                        }

                    break;
                }

                $data['status']  = 200;
                $data['message'] = 'success';
                DB::commit();
                return $data;

            } catch (\Exception $e) {
                return $e;
                DB::rollback();
            }
        }

        return view('admin.login');
    }

    public function draw_result(){
        DB::beginTransaction();
        try {
            // get lucky draw result 
            $lucky_draw_data = LuckyDrawResult::leftJoin('prize_types', 'prize_types.id', '=', 'lucky_draw_results.prize_type_id')
            ->leftJoin('members', 'members.id', '=', 'lucky_draw_results.member_id')
            ->leftJoin('admins', 'admins.id', '=', 'lucky_draw_results.admin_id')
            ->select('members.name as member_name', DB::raw("CONCAT(admins.first_name,' ', admins.last_name) as admin_name"), 'prize_types.name as prize_name', 'lucky_draw_results.winning_number', 'lucky_draw_results.datetime')
            ->get();

            $input = Request::all();
            $filter = [];
            $filter['page']                = ($input['start'] / $input['length']) + 1;
            $filter['search']              = $input['search']['value'];

            if(!$lucky_draw_data){
                $result['recordsTotal']     = 0;
                $result['recordsFiltered']  = 0;
                $result['draw']             = $input['draw'];
                $result['data']             = [];
                return $result;
            }

            $result['recordsTotal']    = LuckyDrawResult::count();
            $result['recordsFiltered'] = LuckyDrawResult::count();
            $result['draw']            = $input['draw'];

            foreach ($lucky_draw_data as $key => $value) {
                $result['data'][$key][] = $value['member_name'];
                $result['data'][$key][] = $value['prize_name'];
                $result['data'][$key][] = $value['admin_name'];
                $result['data'][$key][] = $value['winning_number'];
                $result['data'][$key][] = $value['datetime'];
            }

            DB::commit();
            return json_encode($result);

        } catch (\Exception $e) {
            DB::rollback();
        }

        return json_encode($result);

    }

    public function logout(){
        Request::session()->forget('authentication');
        return redirect('/');
   }
}
