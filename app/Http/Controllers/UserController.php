<?php
namespace App\Http\Controllers;
use App\Models\Polling;
use App\Models\UserPollingOption;
use App\Http\Controllers\Controller;
use Request;
use DB;

class UserController extends Controller
{
    public function answer_poll_question($poll_id){
        DB::beginTransaction();
        try {
            // get selected polling question 
            $polling_data = Polling::where('id', '=', $poll_id)
            ->first();

            $polling_data['options'] = UserPollingOption::where('polling_id', '=', $polling_data['id'])
            ->get();

            // data validation
            if(!$polling_data){
                DB::commit();
                return $this->no_record_message;
            }

            DB::commit();
            return view('user.answer_question', ['polling_data' => $polling_data]);
        } catch (\Exception $e) {
            DB::rollback();
        }

    }
}
