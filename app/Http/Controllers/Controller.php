<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	protected $invalid_password_username = ["status" => 401, "message" => "Invalid Password Or Username."];
	protected $no_record_message 		 = ["status" => 444, "message" => "No records found."];
	protected $no_conform_member	 	 = ["status" => 445, "message" => "No Conform Member found."];
}
