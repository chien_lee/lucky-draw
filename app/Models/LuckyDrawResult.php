<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LuckyDrawResult extends Model
{
    protected $fillable = ['prize_type_id', 'member_id', 'admin_id', 'winning_number', 'datetime'];
}
