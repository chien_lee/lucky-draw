<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberNumber extends Model
{
    protected $fillable = ['member_id', 'number'];
}
