<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function(){
    Route::get('login'                  , 'AdminController@login');
    Route::post('login'                 , 'AdminController@login');
});

Route::group(['middleware' => 'permission'], function(){
    Route::get(''           	    	, 'AdminController@lucky_draw');
    Route::post('draw_process'			, 'AdminController@draw_process');
    Route::get('draw_result'			, 'AdminController@draw_result');
    Route::get('logout'					, 'AdminController@logout');
});
